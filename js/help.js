define(["app", "jquery"], function(app, $) {
  var obj = {};

  obj.checkPlatform = function() {
    var platform = {};
    if (
      document.URL.indexOf("http://") === -1 &&
      document.URL.indexOf("https://") === -1
    ) {
      platform.isphone = true;
    } else {
      platform.isphone = false;
    }
    return platform;
  };

  obj.loadScript = function(url, attrs, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if (attrs) {
      for (prop in attrs) {
        if (!attrs.hasOwnProperty(prop)) continue;
        script.setAttribute(prop, attrs[prop]);
      }
    }
    if (script.readyState) {
      //IE
      script.onreadystatechange = function() {
        if (script.readyState == "loaded" || script.readyState == "complete") {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      //Others
      script.onload = function() {
        callback();
      };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  };

  // Create center toast
  obj.toastAlert = function(info) {
    var toastCenter = app.toast.create({
      text: info,
      position: "center",
      closeTimeout: 2000
    });
    toastCenter.open();
  };

  obj.mGetDate = function(year, month) {
    var d = new Date(year, month, 0);
    return d.getDate();
  };

  //Create center toast
  obj.toastAutoClose = function(info, closeFunction) {
    var toastCenter = app.toast.create({
      text: info,
      position: "center",
      closeTimeout: 1500,
      on: {
        closed: closeFunction
      }
    });
    toastCenter.open();
  };

  obj.showHeader = function(title) {
    $(".header-title").html(title);
    $(".header").removeClass("hide");
    $(".page-content").removeClass("notop");
  };

  obj.hideHeader = function(title) {
    $(".header").addClass("hide");
    $(".page-content").addClass("notop");
  };

  obj.dialogWarning = function(title, text, sureCallback) {
    var dialog = app.dialog.create({
      title: "",
      cssClass: "dialogWarning",
      content:
        "<div><div><div class='title'>" +
        title +
        "</div><div class='close'></div></div><div><div class='icon-warning'></div><div>" +
        text +
        "</div><div><div class='sure'>确定</div><div class='cancel'>取消</div></div></div></div>",
      on: {
        opened: function() {
          $(".dialogWarning .sure").on("click", function() {
            dialog.close();
            if (sureCallback) sureCallback();
          });
          $(".dialogWarning .cancel, .dialogWarning .close").on(
            "click",
            function() {
              dialog.close();
            }
          );
        },
        close: function() {
          $(document).off("click", ".dialogWarning .sure");
          $(document).off("click", ".dialogWarning .cancel");
          $(document).off("click", ".dialogWarning .close");
        },
        closed: function() {}
      }
    });
    dialog.open();
  };

  obj.getMonthValue = function(obj, key) {
    if (obj[key]) {
      return parseFloat(obj[key]);
    }
    return 0;
  };

  obj.isNotEmpty = function(val) {
    if (val === undefined) {
      return false;
    }
    if (val == "" || val == null) {
      return false;
    }
    return true;
  };

  obj.tojsonarray = function() {
    var result = [];
    for (i = 0; i < arguments.length; i++) {
      result.push(arguments[i]);
    }
    return JSON.stringify(result);
  };

  obj.formatDateTime = function(inputTime) {
    var date = new Date(inputTime);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? "0" + m : m;
    var d = date.getDate();
    d = d < 10 ? "0" + d : d;
    var h = date.getHours();
    h = h < 10 ? "0" + h : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? "0" + minute : minute;
    second = second < 10 ? "0" + second : second;
    return y + "-" + m + "-" + d + " " + h + ":" + minute + ":" + second;
  };

  obj.formatSeconds = function(value) {
    var secondTime = parseInt(value); // 秒
    var minuteTime = 0; // 分
    var hourTime = 0; // 小时
    if (secondTime > 60) {
      //如果秒数大于60，将秒数转换成整数
      //获取分钟，除以60取整数，得到整数分钟
      minuteTime = parseInt(secondTime / 60);
      //获取秒数，秒数取佘，得到整数秒数
      secondTime = parseInt(secondTime % 60);
      //如果分钟大于60，将分钟转换成小时
      if (minuteTime > 60) {
        //获取小时，获取分钟除以60，得到整数小时
        hourTime = parseInt(minuteTime / 60);
        //获取小时后取佘的分，获取分钟除以60取佘的分
        minuteTime = parseInt(minuteTime % 60);
      }
    }
    var result = "" + parseInt(secondTime) + "秒";

    if (minuteTime > 0) {
      result = "" + parseInt(minuteTime) + "分" + result;
    }
    if (hourTime > 0) {
      result = "" + parseInt(hourTime) + "小时" + result;
    }
    return result;
  };

  String.prototype.startWith = function(str) {
    var reg = new RegExp("^" + str);
    return reg.test(this);
  };

  Array.prototype.contains = function(needle) {
    for (i in this) {
      if (this[i] == needle) return true;
    }
    return false;
  };

  Array.prototype.startWith = function(needle) {
    for (i in this) {
      var reg = new RegExp("^" + this[i]);
      if (reg.test(needle)) {
        return true;
      }
    }
    return false;
  };

  obj.canotHref = function(info) {
    app.dialog.alert(info);
  };
  obj.add0 = function(m) {
    return m < 10 ? "0" + m : m;
  };

  obj.dateFormat = function(shijianchuo) {
    //shijianchuo是整数，否则要parseInt转换
    var time = new Date(shijianchuo);
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return (
      y +
      "-" +
      add0(m) +
      "-" +
      add0(d) +
      " " +
      add0(h) +
      ":" +
      add0(mm) +
      ":" +
      add0(s)
    );
  };

  obj.toJson = function(object) {
    return JSON.stringify(object);
  };

  Date.prototype.format = function(fmt) {
    var o = {
      "m+": this.getMonth() + 1, //月份
      "d+": this.getDate(), //日
      "h+": this.getHours(), //小时
      "i+": this.getMinutes(), //分
      "s+": this.getSeconds(), //秒
      "q+": Math.floor((this.getMonth() + 3) / 3), //季度
      S: this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        (this.getFullYear() + "").substr(4 - RegExp.$1.length)
      );
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(
          RegExp.$1,
          RegExp.$1.length == 1
            ? o[k]
            : ("00" + o[k]).substr(("" + o[k]).length)
        );
      }
    }
    return fmt;
  };

  obj.testPhone = function(phone) {
    var pattern = "^1[0-9]{10}$";
    var regexp = new RegExp(pattern);
    return regexp.test(phone);
  };

  obj.loadjscssfile = function(filename, filetype) {
    if (filetype == "js") {
      var fileref = document.createElement("script");
      fileref.setAttribute("type", "text/javascript");
      fileref.setAttribute("src", filename);
    } else if (filetype == "css") {
      var fileref = document.createElement("link");
      fileref.setAttribute("rel", "stylesheet");
      fileref.setAttribute("type", "text/css");
      fileref.setAttribute("href", filename);
    }
    //console.log(fileref);
    if (typeof fileref != "undefined")
      document.getElementsByTagName("head")[0].appendChild(fileref);
  };

  obj.localStorageSession = {
    setItem: function(key, jsonData, expirationMilliseconds) {
      var expirationMS = expirationMilliseconds * 1000;
      var record = {
        value: JSON.stringify(jsonData),
        timestamp: new Date().getTime() + expirationMS
      };
      localStorage.setItem(key, JSON.stringify(record));
      return jsonData;
    },
    getItem: function(key) {
      var record = JSON.parse(localStorage.getItem(key));
      if (!record) {
        return false;
      }
      return (
        new Date().getTime() < record.timestamp && JSON.parse(record.value)
      );
    },
    removeItem: function(key) {
      return localStorage.removeItem(key);
    }
  };

  return obj;
});
