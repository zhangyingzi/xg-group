define(["echarts"], function(echarts) {
  var obj = {};
  var color = [
    "#799AF5",
    "#FFC15C",
    "#4BE1BB",
    "#F27968",
    "#9774E9",
    "#61A0A8",
    "#91C7AE",
    "#F6BFAA",
    "#ABE7EA"
  ];

  /**
    字符串模板 模板变量有：
    {a}：系列名。
    {b}：数据名。
    {c}：数据值。
    {d}：百分比。
    {@xxx}：数据中名为'xxx'的维度的值，如{@product}表示名为'product'` 的维度的值。
    {@[n]}：数据中维度n的值，如{@[3]}` 表示维度 3 的值，从 0 开始计数。
   */

  /**
    options = {
            toolTipFormatter: "{a} <br/>{b}: ￥{c} ({d}%)",
            seriesFormatter: "{d|{d}%}\n{c|￥{c}}",
            name: "收入类型-统计",
            data: [
              {
                value: 7000,
                name: "长租车"
              },
              { value: 3000, name: "临停车" }
            ]
          }
  */
  obj.generatePie = function(idElement, options) {
    var myChart = echarts.init(document.getElementById(idElement));

    var legendData = [];
    for (var i = 0; i < options.data.length; i++) {
      legendData.push(options.data[i].name);
    }

    var option = {
      color: color,
      legend: {
        type: "scroll",
        left: "center",
        top: "bottom",
        itemWidth: 12,
        itemHeight: 12,
        textStyle: {
          color: "#9B9B9B",
          fontSize: 12
        },
        data: legendData
      },
      tooltip: {
        trigger: "item",
        formatter: options.toolTipFormatter
          ? options.toolTipFormatter
          : "{a} <br/>{b}: {c} ({d}%)",
        extraCssText: "text-align:center;",
        textStyle: {
          fontWeight: "bold"
        }
      },
      series: [
        {
          name: options.name,
          type: "pie",
          radius: ["30%", "60%"],
          minAngle: 30,
          avoidLabelOverlap: false,
          label: {
            formatter: options.seriesFormatter
              ? options.seriesFormatter
              : "{d|{d}%}\n{c|￥{c}}",
            align: "center",
            color: "#4A4A4A",
            fontWeight: "bold",
            rich: {
              c: {
                color: "#9B9B9B",
                fontSize: 12
              },
              d: {
                color: "#4A4A4A",
                fontWeight: "bold",
                fontSize: 16
              }
            }
          },
          labelLine: {
            show: true,
            lineStyle: {
              color: "#ccc"
            }
          },
          data: options.data
        }
      ]
    };

    myChart.setOption(option, true);
  };

  /**
    options = {
            toolTipFormatter: "{c}辆次<br/>{b}",
            data: [
              ["00:00", 2000],
            ]
          }
  */
  obj.generateLine = function(idElement, options) {
    var myChart = echarts.init(document.getElementById(idElement));

    var xData = [],
      yData = [];
    for (var i = 0; i < options.data.length; i++) {
      xData.push(options.data[i][0]);
      yData.push(options.data[i][1]);
    }

    var option = {
      color: color,
      tooltip: {
        trigger: "axis",
        formatter: options.toolTipFormatter
          ? options.toolTipFormatter
          : "{a} <br/>{b}: {c}",
        extraCssText: "text-align:center;",
        textStyle: {
          fontWeight: "bold"
        }
      },
      grid: {
        containLabel: true,
        top: 20,
        left: "left",
        right: 20,
        bottom: 0
      },
      xAxis: {
        type: "category",
        data: xData,
        splitLine: {
          show: true
        },
        boundaryGap: false,
        axisLine: {
          lineStyle: {
            color: "#4A4A4A",
            fontSize: 12
          }
        },
        axisTick: {
          alignWithLabel: true,
          interval: 0
        }
      },
      yAxis: {
        type: "value",
        splitLine: {
          show: true,
          lineStyle: {
            type: "dashed"
          }
        }
      },
      series: [
        {
          type: "line",
          data: yData,
          name: options.name,
          lineStyle: {
            color: "#5983EF",
            width: 2
          },
          areaStyle: {
            color: {
              type: "linear",
              x: 0,
              y: 0,
              x2: 0,
              y2: 1,
              colorStops: [
                {
                  offset: 0,
                  color: "#D1DDFB"
                },
                {
                  offset: 1,
                  color: "#F6FAFE"
                }
              ],
              global: false
            }
          }
        }
      ]
    };
    myChart.setOption(option, true);
  };

  /**
    options = {
            axisLabelFormatter: "{value}%",
            toolTipFormatter: "{c}辆次<br/>{b}",
            lineNames: ["停车场A"],
            xData: [
              "00:00",
            ],
            yData: [
              [2000],
              [2200],
            ]
          }
  */
  obj.generateMultiLine = function(idElement, options) {
    var myChart = echarts.init(document.getElementById(idElement));

    var series = [];
    for (var i = 0; i < options.yData.length; i++) {
      series.push({
        type: "line",
        name: options.lineNames[i],
        data: options.yData[i],
        lineStyle: {
          color: color[i],
          width: 2
        },
        tooltip: {
          show: true,
          formatter: options.toolTipFormatter
            ? options.toolTipFormatter
            : "{b} <br/>{c}",
          extraCssText: "text-align:center;",
          textStyle: {
            fontWeight: "bold"
          }
        }
      });
    }

    var option = {
      color: color,
      grid: {
        containLabel: true,
        top: 20,
        left: "left",
        right: 20,
        bottom: 40
      },
      tooltip: {
        trigger: "item",
        textAlign: "center"
      },
      legend: {
        show: true,
        align: "right",
        bottom: 0,
        right: 20,
        itemWidth: 20,
        itemHeight: 3
      },
      xAxis: {
        type: "category",
        data: options.xData,
        splitLine: {
          show: true
        },
        boundaryGap: false,
        axisLine: {
          lineStyle: {
            color: "#4A4A4A",
            fontSize: 12
          }
        },
        axisTick: {
          alignWithLabel: true,
          interval: 0
        }
      },
      yAxis: {
        type: "value",
        splitLine: {
          show: true,
          lineStyle: {
            type: "dashed"
          }
        },
        axisLabel: {
          formatter: options.axisLabelFormatter
            ? options.axisLabelFormatter
            : "{value}"
        }
      },
      series: series
    };
    myChart.setOption(option, true);
  };

  /**
    options = {
          axisLabelFormatter: "{value}%",
          labelFormatter:"{@[1]}%",
          data: [["停车场A", 92], ["停车场B", 83], ["停车场C", 76]]
        }
  */
  obj.generateBarHorizontal = function(idElement, options) {
    var myChart = echarts.init(document.getElementById(idElement));

    var option = {
      color: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#5A82F0"
          },
          {
            offset: 1,
            color: "#8FB3F3"
          }
        ],
        global: false
      },
      dataset: {
        source: [["y", "x"]].concat(options.data)
      },
      grid: {
        containLabel: true,
        left: "left",
        top: "top",
        right: 50,
        height: "100%"
      },
      label: {
        show: true,
        position: "right",
        distance: 6,
        formatter: options.labelFormatter ? options.labelFormatter : "{@[1]}",
        color: "#4A4A4A",
        fontSize: 14
      },
      barMaxWidth: 30,
      xAxis: {
        axisLabel: {
          formatter: options.axisLabelFormatter
            ? options.axisLabelFormatter
            : "{value}%"
        },
        splitLine: {
          show: true,
          lineStyle: {
            type: "dashed"
          }
        }
      },
      yAxis: {
        type: "category",
        axisTick: {
          alignWithLabel: true
        }
      },
      series: [
        {
          type: "bar",
          encode: {
            x: "x",
            y: "y"
          }
        }
      ]
    };

    myChart.setOption(option, true);
  };

  /**
    options = {
          axisLabelFormatter: "{value}%",
          labelFormatter:"{@[1]}辆次",
          data: [["停车场A", 92], ["停车场B", 83], ["停车场C", 76]]
        }
  */
  obj.generateBar = function(idElement, options) {
    var myChart = echarts.init(document.getElementById(idElement));

    var option = {
      color: {
        type: "linear",
        x: 0,
        y: 1,
        x2: 0,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#5A82F0"
          },
          {
            offset: 1,
            color: "#8FB3F3"
          }
        ],
        global: false
      },
      dataset: {
        source: [["x", "y"]].concat(options.data)
      },
      grid: {
        containLabel: true,
        left: "left",
        top: 20,
        bottom: 0,
        width: "100%"
      },
      label: {
        show: true,
        position: "top",
        distance: 6,
        formatter: options.labelFormatter ? options.labelFormatter : "{@[1]}",
        color: "#4A4A4A",
        fontSize: 14
      },
      barMaxWidth: 60,
      xAxis: {
        type: "category",
        axisTick: {
          alignWithLabel: true
        },
        axisLabel: {
          //interval: 0,
          //rotate: 0,
          fontWeight: "normal",
          formatter: function(value) {
            var ret = "";
            var maxLength = 6; //每项显示文字个数
            var valLength = value.length;
            var rowN = Math.ceil(valLength / maxLength);
            if (rowN > 1) {
              for (var i = 0; i < rowN; i++) {
                var temp = "";
                var start = i * maxLength;
                var end = start + maxLength;
                temp = value.substring(start, end) + "\n";
                ret += temp;
              }
              return ret;
            } else {
              return value;
            }
          }
        }
      },
      yAxis: {
        axisLabel: {
          formatter: options.axisLabelFormatter
            ? options.axisLabelFormatter
            : "{value}"
        },
        splitLine: {
          show: true,
          lineStyle: {
            type: "dashed"
          }
        }
      },
      series: [
        {
          type: "bar",
          encode: {
            x: "x",
            y: "y"
          }
        }
      ]
    };

    myChart.setOption(option, true);
  };

  return obj;
});
