requirejs.config({
  baseUrl: "./lib/",
  paths: {
    jquery: "js/jquery-3.3.1",
    Framework7: "js/framework7.v4.1.1.min",
    echarts: "js/echarts.min",
    chart: "../js/chart",
    config: "../config",
    routes: "../js/routes",
    app: "../js/app",
    help: "../js/help",
    Paging: "../js/paging",
    data: "../js/data",
    css: "js/css.min",
    bluebird: "js/bluebird.min",
    mobile: "../js/mobile",
    CustomComponentJS: "../js/CustomComponent"
  },
  shim: {
    chart: ["echarts"]
  }
});

requirejs(["jquery", "help", "Framework7", "app", "data", "mobile"], function(
  $,
  help,
  Framework7,
  app,
  data
) {
  //var $$ = Dom7;

  if (help.checkPlatform().isphone) {
    help.loadScript("cordova.js", {}, function() {
      document.addEventListener(
        "deviceready",
        function() {
          //navigator.splashscreen.hide();
          // if (device.platform.toLowerCase() == "ios") {
          // } else {
          // }
          // if (device.platform.toLowerCase() == "android") {
          // } else {
          // }
        },
        false
      );
    });
  }

  var mainView = app.views.create(".view-main", {
    url: "/ty",
    on: {
      pageInit: function() {
        var url = this.router.url;
        if (url == "/") {
        }
      }
    }
  });

  $(".header-back").on("click", function() {
    app.views.main.router.back({ force: true });
  });

  app.views.main.router.navigate("/index", {
    reloadCurrent: true
  });
});
