(function(doc, win, designWidth, minWidth, maxWidth) {
  var browser = {
    versions: (function() {
      var u = navigator.userAgent;
      return {
        //移动终端浏览器版本信息
        trident: u.indexOf("Trident") > -1, //IE内核
        presto: u.indexOf("Presto") > -1, //opera内核
        webKit: u.indexOf("AppleWebKit") > -1, //苹果、谷歌内核
        gecko: u.indexOf("Gecko") > -1 && u.indexOf("KHTML") == -1, //火狐内核
        mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
        android: u.indexOf("Android") > -1 || u.indexOf("Linux") > -1, //android终端或uc浏览器
        iPhone: u.indexOf("iPhone") > -1, //是否为iPhone或者QQHD浏览器
        iPad: u.indexOf("iPad") > -1, //是否iPad
        webApp: u.indexOf("Safari") == -1 //是否web应该程序，没有头部与底部
      };
    })(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
  };

  var docEl = doc.documentElement,
    docElWidth = docEl.clientWidth,
    isIOS = navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
    dpr = win.devicePixelRatio,
    dpr = browser.versions.android ? 1 : window.top === window.self ? dpr : 1,
    scale = 1 / dpr,
    resizeEvt = "orientationchange" in window ? "orientationchange" : "resize";
  var recalc = function() {
    docElWidth = docEl.clientWidth;
    var width = docElWidth;
    if (width > maxWidth) {
      width = maxWidth;
    }
    if (width < minWidth) {
      width = minWidth;
    }
    docEl.dataset.dpr = dpr;
    docEl.dataset.width = width;
    docEl.dataset.rem = 100 * (width / designWidth);
    docEl.style.fontSize = 100 * (width / designWidth) + "px";
  };

  recalc();
  if (!doc.addEventListener) return;
  win.addEventListener(resizeEvt, recalc, false);
})(document, window, 750, 320, 414);
