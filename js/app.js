define(["Framework7", "routes"], function(Framework7, routes) {
  var $$ = Dom7;

  var app = new Framework7({
    root: "#app",
    id: "com.ruolinlink.xt",
    name: "国网信通",
    theme: "md",
    cache: false,
    data: function() {
      return {};
    },
    on: {
      routeChange: function(newRoute, previousRoute, router) {
        var currentUrl = newRoute.url;
      }
    },
    dialog: {
      title: "国网信通",
      buttonOk: "确定",
      buttonCancel: "取消"
    },
    methods: {
      helloWorld: function() {
        app.dialog.alert("Hello World!");
      }
    },
    routes: routes,
    panel: {
      //leftBreakpoint: 960
    }
  });

  return app;
});
