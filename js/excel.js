//define(["JsonExportExcel"], function(ExportJsonExcel) {
define([], function() {
  var obj = {};

  obj.toExcel = function(title, datas) {
    var option = {
      fileName: title,
      datas: [
        {
          sheetData: datas
        }
      ]
    };
    var toExcel = new ExportJsonExcel(option);
    toExcel.saveExcel();
  };

  return obj;
});
