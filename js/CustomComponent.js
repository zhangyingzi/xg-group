define([], function() {
  this.CustomComponentJS = this.CustomComponentJS || {};

  CustomComponentJS.ProjectUtil = (function() {
    var instantiated;

    function init() {
      var op = Object.prototype;

      var ostring = op.toString;

      /**
       *  判断字符串是否是JSON格式
       **/
      function stringIsJSONHandler(paramStr) {
        paramStr = String(paramStr);

        if (
          paramStr == null ||
          stringTrim(paramStr).length == 0 ||
          paramStr.toLowerCase() == "null"
        ) {
          return false;
        }

        var paramStrLength = paramStr.length;

        var startChat = String(paramStr.charAt(0));
        var endChat = String(paramStr.charAt(paramStrLength - 1));

        if (
          (startChat == "{" && endChat == "}") ||
          (startChat == "[" && endChat == "]")
        ) {
          return true;
        }

        return false;
      }

      /**
       *  判断字符串是否是为空
       **/
      function stringIsNULLHandler(paramStr) {
        paramStr = String(paramStr);

        if (
          paramStr == null ||
          stringTrim(paramStr).length == 0 ||
          paramStr.toLowerCase() == "null"
        ) {
          return true;
        }

        return false;
      }

      /**
       *  判断变量是否是为空
       **/
      function variableIsNULLHandler(paramVariable) {
        if (paramVariable == null || paramVariable == undefined) {
          return true;
        }

        return false;
      }

      function stringTrim(x) {
        return x.replace(/^\s+|\s+$/gm, "");
      }

      /**
       * 判断是否是函数
       **/
      function isFunctionHandler(it) {
        return ostring.call(it) === "[object Function]";
      }

      /**
       * 判断是否是数组
       **/
      function isArrayHandler(it) {
        return ostring.call(it) === "[object Array]";
      }

      /**
       * 判断是否是字符型
       **/
      function isStringHandler(param) {
        if (typeof param === "string") {
          return true;
        } else {
          return false;
        }
      }

      /**
       *  角度转换成弧度
       **/
      function angleToRadianHandler(angleNum) {
        var resultNum = (Math.PI / 180) * angleNum;

        return resultNum;
      }

      /**
       *  角度转换成弧度
       **/
      function radianToAngleHandler(radianNum) {
        var resultNum = (180 / Math.PI) * radianNum;

        return resultNum;
      }

      return {
        variableIsNULLHandler: variableIsNULLHandler,
        stringIsNULLHandler: stringIsNULLHandler,
        stringIsJSONHandler: stringIsJSONHandler,
        isFunctionHandler: isFunctionHandler,
        isArrayHandler: isArrayHandler,
        angleToRadianHandler: angleToRadianHandler,
        radianToAngleHandler: radianToAngleHandler,
        isStringHandler: isStringHandler
      };
    }

    return {
      getInstance: function() {
        if (!instantiated) {
          instantiated = init();
        }
        return instantiated;
      }
    };
  })();

  (function() {
    function Gauge() {
      // 当前值
      this.curVal = 0;

      // 总的值
      this.sumVal = 100;

      // 当前的Canvas
      this.targetCanvas = null;

      // 标题文本
      this.titleTxt = "";

      // 标题文字大小
      this.titleFontSize = "";

      // 标题文字字体
      this.titleFontFamily = "微软雅黑";

      // 标题文字字体
      this.titleColor = "#ffffff";

      // 值文字大小
      this.valueFontSize = "";

      // 值文字字体
      this.valueFontFamily = "微软雅黑";

      this.valueColor = "#ffffff";

      // 符号文本
      this.symbolTxt = "";

      // 符号文字大小
      this.symbolFontSize = "";

      // 符号文字字体
      this.symbolFontFamily = "微软雅黑";

      this.symbolColor = "#ffffff";

      this.radius = 0;

      this.arcThickness = 0;

      this.bgFillColor = "#0078d2";

      this.bgBorderColor = "#0078d2";

      this.centerBgFillColor = "#0064b4";

      this.centerBgBorderColor = "#0064b4";

      this.valueFillColorArray = [
        { ratios: 0, color: "#12fff7" },
        { ratios: 0.2, color: "#12fff7" },
        { ratios: 1, color: "#b3ffab" }
      ];

      this.centerPoint = null;

      this.visibleTxtFlag = false;
    }

    var p = Gauge.prototype;

    p.init = function(param) {
      if (gaugeTargetCanvas == null) {
        gaugeTargetCanvas = document.createElement("canvas");
        document.body.appendChild(gaugeTargetCanvas);
      }

      gaugeTargetCanvas.setAttribute("width", this.radius * 2);
      gaugeTargetCanvas.setAttribute("height", this.radius * 2);

      var ctx = gaugeTargetCanvas.getContext("2d");

      var radius = this.radius;

      var arcThickness = this.arcThickness;

      var startPoint = new Object();

      startPoint.x = 0 + radius;
      startPoint.y = 0 + radius;

      this.centerPoint = startPoint;

      // 背景圆
      this.drawCircleHandler(
        ctx,
        startPoint.x,
        startPoint.y,
        radius,
        this.bgBorderColor,
        0,
        this.bgFillColor
      );

      var fillColorArray = this.valueFillColorArray;

      var valRatio = this.curVal / this.sumVal;

      var valAngleNum = 360 * valRatio;

      var startAngleNum = -90;

      valAngleNum = valAngleNum + startAngleNum;

      var valueStartPoint = new Object();

      valueStartPoint.x = this.radius;
      valueStartPoint.y = arcThickness / 2;

      var valueEndPoint = new Object();

      valueEndPoint.x = this.radius;
      valueEndPoint.y = 0;

      // ===============================值圆=======================start========================================

      // this.drawArcHandler(ctx,startPoint.x,startPoint.y,radius,fillColorArray,0,fillColorArray,CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(startAngleNum),CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(valAngleNum));
      //
      // console.log("valueStartPoint.y=========================="+valueStartPoint.y)
      //
      // this.drawCircleHandler(ctx,valueStartPoint.x,valueStartPoint.y,valueStartPoint.y,fillColorArray[0].color,0,fillColorArray[0].color);

      this.drawValueHandler(
        ctx,
        startPoint.x,
        startPoint.y,
        radius,
        fillColorArray,
        0,
        fillColorArray,
        startAngleNum,
        valAngleNum,
        arcThickness
      );

      // ===============================值圆=======================end========================================

      this.drawCircleHandler(
        ctx,
        startPoint.x,
        startPoint.y,
        radius - arcThickness,
        this.centerBgBorderColor,
        0,
        this.centerBgFillColor
      );

      if (this.visibleTxtFlag) {
        var valTxtStartNum = 10;

        this.generateTextHandler(
          ctx,
          this.curVal,
          this.valueColor,
          this.valueFontSize,
          this.valueFontFamily,
          "center",
          -27 + valTxtStartNum,
          60
        );

        this.generateTextHandler(
          ctx,
          this.symbolTxt,
          this.symbolColor,
          this.symbolFontSize,
          this.symbolFontFamily,
          "center",
          22 + valTxtStartNum,
          47
        );

        this.generateTextHandler(
          ctx,
          this.titleTxt,
          this.titleColor,
          this.titleFontSize,
          this.titleFontFamily,
          "center",
          0,
          -9
        );
      }
    };

    p.drawValueHandler = function(
      ctx,
      x,
      y,
      radius,
      borderColor,
      thickness,
      fillColor,
      startAngle,
      endAngle,
      arcThickness
    ) {
      ctx.beginPath();

      var startPoint = new Object();
      startPoint.x = x;
      startPoint.y = y;

      var realAngleNum = endAngle - startAngle;

      if (realAngleNum >= 360) {
        var offAngleNum = realAngleNum - 360;
        endAngle = endAngle - offAngleNum - 1;
      }

      var startPointCenterPoint = this.drawValueStartPointHandler(
        ctx,
        radius,
        startAngle,
        startPoint,
        arcThickness
      );

      this.drawPointHander(
        ctx,
        radius,
        startAngle,
        endAngle,
        0,
        startPoint,
        true
      );

      var endPointCenterPoint = this.drawValueEndPointHandler(
        ctx,
        radius,
        endAngle,
        startPoint,
        arcThickness
      );

      this.drawPointHander(
        ctx,
        radius - arcThickness,
        endAngle,
        startAngle,
        0,
        startPoint
      );

      this.generateFillColorHandler(
        ctx,
        fillColor,
        startPointCenterPoint,
        endPointCenterPoint
      );

      ctx.fill();
      ctx.closePath();
    };

    p.drawValueStartPointHandler = function(
      ctx,
      radius,
      startAngle,
      startPoint,
      arcThickness
    ) {
      var startPointRadius = radius - arcThickness / 2;

      var startPointCenterPoint = new Object();

      startPointCenterPoint.x =
        startPointRadius *
        Math.cos(
          CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(
            startAngle
          )
        );
      startPointCenterPoint.y =
        startPointRadius *
        Math.sin(
          CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(
            startAngle
          )
        );

      startPointCenterPoint.x = startPointCenterPoint.x + startPoint.x;

      startPointCenterPoint.y = startPointCenterPoint.y + startPoint.y;

      this.drawPointHander(
        ctx,
        arcThickness / 2,
        0,
        360,
        0,
        startPointCenterPoint,
        false
      );

      return startPointCenterPoint;
    };

    p.drawValueEndPointHandler = function(
      ctx,
      radius,
      endAngle,
      startPoint,
      arcThickness
    ) {
      var endPointRadius = radius - arcThickness / 2;

      var endPointCenterPoint = new Object();

      endPointCenterPoint.x =
        endPointRadius *
        Math.cos(
          CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(
            endAngle
          )
        );
      endPointCenterPoint.y =
        endPointRadius *
        Math.sin(
          CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(
            endAngle
          )
        );

      endPointCenterPoint.x = endPointCenterPoint.x + startPoint.x;

      endPointCenterPoint.y = endPointCenterPoint.y + startPoint.y;

      this.drawPointHander(
        ctx,
        arcThickness / 2,
        0,
        360,
        0,
        endPointCenterPoint,
        false
      );

      return endPointCenterPoint;
    };

    p.drawPointHander = function(
      ctx,
      radius,
      startAngle,
      endAngle,
      offAngle,
      centerPoint,
      firstFlag
    ) {
      var startAngleNum = startAngle - offAngle;

      var endAngleNum = endAngle - offAngle;

      if (startAngle < endAngle) {
        for (var i = startAngleNum; i <= endAngleNum; i++) {
          this.generatePointHandler(
            ctx,
            i,
            radius,
            centerPoint,
            startAngleNum,
            firstFlag
          );
        }
      } else {
        for (var i = startAngleNum; i >= endAngleNum; i--) {
          this.generatePointHandler(
            ctx,
            i,
            radius,
            centerPoint,
            startAngleNum,
            firstFlag
          );
        }
      }
    };

    p.generatePointHandler = function(
      ctx,
      angleNum,
      radius,
      centerPoint,
      startAngleNum,
      firstFlag
    ) {
      var x =
        radius *
        Math.cos(
          CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(
            angleNum
          )
        );
      var y =
        radius *
        Math.sin(
          CustomComponentJS.ProjectUtil.getInstance().angleToRadianHandler(
            angleNum
          )
        );

      var startPoint = centerPoint;

      x = startPoint.x + x;
      y = startPoint.y + y;

      if (angleNum == startAngleNum) {
        if (firstFlag) {
          ctx.moveTo(x, y);
        } else {
          ctx.lineTo(x, y);
        }
      } else {
        ctx.lineTo(x, y);
      }
    };

    p.generateTextHandler = function(
      ctx,
      contextTxt,
      color,
      fontSize,
      fontFamily,
      position,
      offXNum,
      OffYNum
    ) {
      ctx.fillStyle = color;
      ctx.font = fontSize + " " + fontFamily; //设置字体
      ctx.textBaseline = "alphabetic"; //在绘制文本时使用的当前文本基线

      var x = 0;
      var y = 0;

      if (position == "left") {
      } else if (position == "center") {
        var textMetrics = ctx.measureText(contextTxt);

        var contextTxtWidth = textMetrics.width;
        var contextTxtHeight = fontSize.replace(/px/g, "");

        if (isNaN(contextTxtHeight)) {
          contextTxtHeight = 0;
        }

        x = this.centerPoint.x - contextTxtWidth / 2;
        y = this.centerPoint.y - contextTxtHeight / 2;
      } else if (position == "rigth") {
        var contextTxtWidth = ctx.measureText(this.contextTxt).width;
        x = this.radius * 2 - contextTxtWidth;
      }

      x = x + offXNum;
      y = y + OffYNum;

      ctx.fillText(contextTxt, x, y); //设置文本内容
    };

    p.drawCircleHandler = function(
      ctx,
      x,
      y,
      radius,
      borderColor,
      thickness,
      fillColor
    ) {
      if (thickness > 0) {
        ctx.strokeStyle = borderColor;
        ctx.lineWidth = thickness;
      }

      ctx.beginPath();
      ctx.arc(x, y, radius, 0, 2 * Math.PI);
      this.generateFillColorHandler(ctx, fillColor, null, null);
      ctx.fill();
      if (thickness > 0) {
        ctx.stroke();
      }
      ctx.closePath();
    };

    p.drawArcHandler = function(
      ctx,
      x,
      y,
      radius,
      borderColor,
      thickness,
      fillColor,
      startAngle,
      endAngle
    ) {
      if (thickness > 0) {
        ctx.strokeStyle = borderColor;
        ctx.lineWidth = thickness;
      }

      ctx.beginPath();
      ctx.arc(x, y, radius, startAngle, endAngle);
      this.generateFillColorHandler(ctx, fillColor, null, null);
      ctx.fill();
      if (thickness > 0) {
        ctx.stroke();
      }
      ctx.closePath();
    };

    p.drawRectHandler = function(
      ctx,
      x,
      y,
      width,
      height,
      borderColor,
      thickness,
      fillColor
    ) {
      if (thickness > 0) {
        ctx.strokeStyle = borderColor;
        ctx.lineWidth = thickness;
      }

      ctx.beginPath();
      this.generateFillColorHandler(ctx, fillColor, null, null);
      ctx.fillRect(x, y, width, height);
      if (thickness > 0) {
        ctx.stroke();
      }
      ctx.closePath();
    };

    p.generateFillColorHandler = function(
      ctx,
      fillColor,
      startPoint,
      endPoint
    ) {
      if (
        CustomComponentJS.ProjectUtil.getInstance().isArrayHandler(fillColor)
      ) {
        //创建线性渐变的方法->CanvasGradient类型的对象
        var linearGradient = null;
        if (startPoint != null && endPoint != null) {
          linearGradient = ctx.createLinearGradient(
            startPoint.x,
            startPoint.y,
            endPoint.x,
            endPoint.y
          );
        } else {
          linearGradient = ctx.createLinearGradient(100, 0, 100, 100);
        }

        for (var i = 0; i < fillColor.length; i++) {
          var fillColorObj = fillColor[i];
          linearGradient.addColorStop(fillColorObj.ratios, fillColorObj.color);
        }
        ctx.fillStyle = linearGradient;
      } else {
        ctx.fillStyle = fillColor;
      }
    };

    CustomComponentJS.Gauge = Gauge;
  })();

  (function() {
    var minValue = 0;

    var maxValue = 500;

    var perValue = 0;

    function PMGauge() {
      this.valueImageCount = 19;

      this.valueImagePathArray = [];

      this.targetCanvas = null;

      perValue = (maxValue - minValue) / this.valueImageCount;

      this.contentWidth = 0;

      this.contentHeight = 0;

      for (var i = 0; i < this.valueImageCount; i++) {
        if (i < 10) {
          this.valueImagePathArray.push(
            "./assets/duu/02_02_0" + i + "_@3x.png"
          );
        } else {
          this.valueImagePathArray.push("./assets/duu/02_02_" + i + "_@3x.png");
        }
      }
    }

    var p = PMGauge.prototype;

    p.setValueHandler = function(curVal) {
      if (this.targetCanvas == null) {
        this.targetCanvas = document.createElement("canvas");
        document.body.appendChild(this.targetCanvas);
      }
      var gaugeTargetCanvas = this.targetCanvas;

      gaugeTargetCanvas.setAttribute("width", this.contentWidth);
      gaugeTargetCanvas.setAttribute("height", this.contentHeight);

      var ctx = gaugeTargetCanvas.getContext("2d");
      var index = (curVal / perValue).toFixed(0);

      if (isNaN(index)) {
        index = 0;
      }

      index = parseInt(index) + 1;

      var img = new Image();

      var _that = this;

      img.onload = function(e) {
        // 将图片画到canvas上面上去！
        ctx.drawImage(img, 0, 0, _that.contentWidth, _that.contentHeight);
      };
      img.src = this.valueImagePathArray[index];
    };

    CustomComponentJS.PMGauge = PMGauge;
  })();

  return this.CustomComponentJS;
});
