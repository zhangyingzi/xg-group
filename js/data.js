define(["app", "config", "help", "Framework7", "bluebird"], function(
  app,
  config,
  help,
  Framework7,
  Promise
) {
  var obj = {};

  var isShowAlert = false;
  obj.request = function(method, url, data, callback) {
    app.preloader.show();
    var options = {
      url: config.service.PRIX + url,
      method: method,
      contentType: "application/x-www-form-urlencoded",
      async: true,
      data: data,
      timeout: 5000,
      processData: true,
      success: function(successData) {
        if (callback) {
          callback(null, successData ? JSON.parse(successData) : null);
        }
      },
      error: function(xhr, httpErrorCode) {
        if (xhr.status == 0) {
          if (!isShowAlert) {
            isShowAlert = true;
            app.dialog.alert("服务响应超时,请稍后再试", function() {
              isShowAlert = false;
            });
          }
        } else if (httpErrorCode == 401) {
          // Unauthorized - 用户没有权限（用户名、密码错误）
          if (!isShowAlert) {
            isShowAlert = true;
            app.dialog.alert("用户没有权限（用户名、验证码错误）", function() {
              isShowAlert = false;
              window.localStorage.clear();
              window.location.reload();
            });
          }
        } else if (httpErrorCode == 404) {
          // 404 Not Found
          if (!isShowAlert) {
            isShowAlert = true;
            app.dialog.alert("404 Not Found", function() {
              isShowAlert = false;
            });
          }
        } else {
          if (callback) {
            callback({ code: httpErrorCode, message: xhr.responseText });
          }
        }
      },
      complete: function() {
        app.preloader.hide();
      }
    };
    if (window.localStorage.token) {
      options.headers = {
        Authentication: "Bearer " + window.localStorage.token
      };
    }
    app.request(options);
  };

  obj.logout = function() {
    obj.request(
      "DELETE",
      "/sys/auth",
      { token: window.localStorage.token },
      function(error, successData) {
        if (error) {
          app.dialog.alert(error.message);
          return;
        }
        window.localStorage.clear();
        help.toastAutoClose("退出登录成功", function() {
          //window.location.reload();
          app.views.main.router.navigate("/person/login", {
            reloadCurrent: true
          });
        });
      }
    );
  };

  // 集团所有停车场
  obj.getGroupParkings = function(groupId) {
    return new Promise(function(resolve, reject) {
      if (Template7.global && Template7.global.groupParkings) {
        return resolve(Template7.global.groupParkings);
      }
      obj.request(
        "GET",
        "/grp/" + groupId + "/parking",
        {
          grpId: groupId
        },
        function(error, successData) {
          if (error) {
            return reject(error);
          }
          var list = successData.list;
          help.setGlobalContext("groupParkings", list);
          return resolve(list);
        }
      );
    });
  };

  return obj;
});
