define(function() {
  return [
    {
      path: "/",
      url: "./index.html"
    },
    {
      path: "/ty",
      componentUrl: "./pages/ty/ty.html"
    },
    {
      path: "/index",
      componentUrl: "./pages/index/index.html"
    },
    {
      path: "/networkConnections",
      componentUrl: "./pages/networkConnections.html"
    },
    {
      path: "/parkLocation",
      componentUrl: "./pages/parkLocation.html"
    },
    {
      path: "/venueIntroduction",
      componentUrl: "./pages/venueIntroduction.html"
    },
    {
      path: "/open/:url",
      componentUrl: "./pages/index/open.html"
    },
    {
      path: "/cupon/generate",
      componentUrl: "./pages/cupon/generate.html",
      options: {
        reloadCurrent: true
      }
    },
    {
      path: "(.*)",
      url: "./pages/404.html"
    }
  ];
});
