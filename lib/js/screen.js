function setPageContentHeightReduce(reduce){
	var winHeight=0;
	var winWidth=0;
	 //获取窗口宽度
    if (window.innerWidth) {
        winWidth = window.innerWidth;
    }
    else if ((document.body) && (document.body.clientWidth)) {
        winWidth = document.body.clientWidth;
    }

    //获取窗口高度
    if (window.innerHeight) {
        winHeight = window.innerHeight;
    } else if ((document.body) && (document.body.clientHeight)){
        winHeight = document.body.clientHeight;
    }

    //通过深入Document内部对body进行检测，获取窗口大小
    if (document.documentElement && document.documentElement.clientHeight && document.documentElement.clientWidth) {

        winHeight=document.documentElement.clientHeight;

        winWidth=document.documentElement.clientWidth;
    }
    
    if(winWidth>1024){
    	$$(".page-content").css("height",winHeight-reduce+"px");
    	$$(".login-screen-content").css("height",winHeight-200+"px");
    	$$(".login-screen-content").css("marginTop",100+"px");
    }
    
}

function setPageContentMarginTop(marginTop){
	var winHeight=0;
	var winWidth=0;
	 //获取窗口宽度
    if (window.innerWidth) {
        winWidth = window.innerWidth;
    }
    else if ((document.body) && (document.body.clientWidth)) {
        winWidth = document.body.clientWidth;
    }

    //获取窗口高度
    if (window.innerHeight) {
        winHeight = window.innerHeight;
    } else if ((document.body) && (document.body.clientHeight)){
        winHeight = document.body.clientHeight;
    }

    //通过深入Document内部对body进行检测，获取窗口大小
    if (document.documentElement && document.documentElement.clientHeight && document.documentElement.clientWidth) {

        winHeight=document.documentElement.clientHeight;

        winWidth=document.documentElement.clientWidth;
    }
    
    if(winWidth>1024){
    	$$(".page-content").css("marginTop",marginTop+"px");
    }else{
    	if(app.device.desktop){
    		$$(".page-content").css("marginTop",marginTop+"px");
    	}
    }
}


setPageContentHeightReduce(110);
setPageContentMarginTop(76);

var haveSearchBarUrl = Array(
		"/public-parking/list",
		"/lock/list",
		"/lot-batch/list",
		"/public-parking-lot/list/",
		"/error-lot/list",
		"/error-lot/city",
		"/message/lock-list"
);

function autoGenPageContentSize(currentUrl){
	var winHeight=0;
	var winWidth=0;
	 //获取窗口宽度
    if (window.innerWidth) {
        winWidth = window.innerWidth;
    }
    else if ((document.body) && (document.body.clientWidth)) {
        winWidth = document.body.clientWidth;
    }

    //获取窗口高度
    if (window.innerHeight) {
        winHeight = window.innerHeight;
    } else if ((document.body) && (document.body.clientHeight)){
        winHeight = document.body.clientHeight;
    }

    //通过深入Document内部对body进行检测，获取窗口大小
    if (document.documentElement && document.documentElement.clientHeight && document.documentElement.clientWidth) {

        winHeight=document.documentElement.clientHeight;

        winWidth=document.documentElement.clientWidth;
    }
    
    if(winWidth>1024){
    	if(haveSearchBarUrl.contains(currentUrl)||haveSearchBarUrl.startWith(currentUrl)){
    		setPageContentMarginTop(120);
    		setPageContentHeightReduce(158);
    	}else if(currentUrl!="/"){
    		setPageContentMarginTop(76);
    		setPageContentHeightReduce(110);
    	}
    }
}


